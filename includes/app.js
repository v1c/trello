/** 
* Copyright 2018 Victor Meyer All rights reserved.
*/

"use strict";

var app = angular.module('trello', ['ui.sortable']);

app.controller('Boards', function ($scope) 
{
	
	// variables
	$scope.boardIndex = 0;
	$scope.boards = [];
	$scope.errorMessage = "";
	
	// navigation
	$scope.changeBoard = function(index)
	{
		$scope.errorMessage = "";
		$scope.boardIndex = index;
	}

	// boards
	$scope.addBoard = function()
	{
		var board = new Board("");
		board.edit = true;
		
		$scope.boards.push(board);
		$scope.changeBoard($scope.boards.length - 1); 
	}
	
	$scope.editBoard = function(board)
	{
		board.edit = true;
		board.tmp = board.name;
	}
	
	$scope.saveBoard = function(board)
	{
		if (!$scope.validName(board.tmp, "Board")) return;
		if ($scope.alreadyExists($scope.boards, board.tmp, "Board")) return;
		
		board.name = board.tmp;
		board.edit = false;
	}
	
	$scope.cancelBoard = function(board)
	{
		if (board.name == "")
		{			
			// delete
			$scope.boards.splice($scope.boards.indexOf(board), 1);
			$scope.boardIndex = 0;
		}
		else
		{
			// cancel edit
			board.edit = false;
		}
	}
	
	// lists
	$scope.addList = function()
	{
		var list = new List("");
		list.edit = true;
		
		$scope.boards[$scope.boardIndex].lists.push(list);
	}
	
	$scope.editList = function(list)
	{
		list.edit = true;
		list.tmp = list.name;
	}
	
	$scope.saveList = function(list)
	{
		if (!$scope.validName(list.tmp, "List")) return;
		if ($scope.alreadyExists($scope.boards[$scope.boardIndex].lists, list.tmp, "List")) return;
		
		list.name = list.tmp;
		list.edit = false;
	}
	
	$scope.cancelList = function(list)
	{
		if (list.name == "")
		{
			// delete
			var i = $scope.boards[$scope.boardIndex].lists.indexOf(list);
			$scope.boards[$scope.boardIndex].lists.splice(i, 1);
		}
		else
		{
			// cancel edit
			list.edit = false;
		}
	}
	
	// cards
	$scope.addCard = function(index, name)
	{
		var card = new Card("");
		card.edit = true;
		
		$scope.boards[$scope.boardIndex].lists[index].cards.push(card);
	}
	
	$scope.editCard = function(card)
	{
		card.edit = true;
		card.tmp = card.name;
	}
	
	$scope.saveCard = function(index, card)
	{
		if (!$scope.validName(card.tmp, "Card")) return;
		if ($scope.alreadyExists($scope.boards[$scope.boardIndex].lists[index].cards, card.tmp, "Card")) return;
		
		card.name = card.tmp;
		card.edit = false;
	}
	
	$scope.cancelCard = function(index, card)
	{
		if (card.name == "")
		{
			// delete card
			var i = $scope.boards[$scope.boardIndex].lists[index].cards.indexOf(card);
			$scope.boards[$scope.boardIndex].lists[index].cards.splice(i, 1);
		}
		else
		{
			// cancel edit
			card.edit = false;
		}
	}
	
	// validation
	$scope.validName = function(name, type)
	{
		if (name.match(/^[a-zA-Z0-9 ]{1,30}$/))
		{
			$scope.errorMessage = "";
			return true;
		}
		else
		{
			$scope.errorMessage = "Invalid " + type + " Name (Alphanumeric and Spaces Only)";
			return false;
		}
	}
	
	$scope.alreadyExists = function(list, name, type)
	{
		var exists = false;
		
		angular.forEach(list, function(item) 
		{
			if (name == item.name) 
			{
				exists = true;
			}
		});
		
		$scope.errorMessage = (exists) ? type + " already exists." : "";
		
		return exists;
	}
	
	// drag & drop
	$scope.sortableOptions = 
	{
		placeholder: "card",
		connectWith: ".cardListing",
		update: function(e, ui) 
		{
			if (!ui.item.sortable.received) 
			{
				// ignore same board movement
				if (ui.item.sortable.droptargetModel === ui.item.sortable.sourceModel) return;
				
				// stop duplicates
	        		if ($scope.alreadyExists(ui.item.sortable.droptargetModel, ui.item.sortable.sourceModel[ui.item.sortable.index].name, "Card")) 
	        		{
	        			ui.item.sortable.cancel(); 
	        		}
			}
		}
	};
	
	// start with prepopulated boards
	$scope.prePopulate = function()
	{
		// board 1
		var board = new Board("Board 1");
		$scope.boards.push(board);
		
		var list = new List("List 1");
		board.lists.push(list);
		
		var card = new Card("Card 1");
		list.cards.push(card);
		
		var card2 = new Card("Card 2");
		list.cards.push(card2);
		
		var list1 = new List("List 2");
		board.lists.push(list1);
		
		var card3 = new Card("Card 1");
		list1.cards.push(card3);
		
		// board 2
		var board2 = new Board("Board 2");
		$scope.boards.push(board2);
		
		var list2 = new List("List 1");
		board2.lists.push(list2);
		
		var card4 = new Card("Card 1");
		list2.cards.push(card4);
	}
	$scope.prePopulate();
	
})

// classes for board/list/card
function Board(name)
{
	this.name = name;
	this.tmp = "";
	this.edit = false;
	this.lists = [];
}

function List(name)
{
	this.name = name;
	this.tmp = "";
	this.edit = false;
	this.cards = [];
}

function Card(name)
{	
	this.name = name;
	this.tmp = "";
	this.edit = false;
}
